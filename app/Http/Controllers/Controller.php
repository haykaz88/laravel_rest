<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    const STATUS_OK = 200;
    const STATUS_NO_CONTENT = 204;
    const STATUS_NOT_MODIFIED = 304;
    const STATUS_BAD_REQUEST = 400;
    const STATUS_UNAUTHORIZED = 401;
    const STATUS_FORBIDDEN = 403;
    const STATUS_NOT_FOUND = 404;
    const STATUS_UNPROCESSABLE_ENTITY = 422;
    
    private function getMessages() {
        return [
            self::STATUS_OK => 'OK.',
            self::STATUS_NO_CONTENT => 'No Content.',
            self::STATUS_NOT_MODIFIED => 'Not Modified.',
            self::STATUS_BAD_REQUEST => 'Bad Request.',
            self::STATUS_UNAUTHORIZED => 'Unauthorized.',
            self::STATUS_FORBIDDEN => 'Forbidden.',
            self::STATUS_NOT_FOUND => 'Not Found.',
            self::STATUS_UNPROCESSABLE_ENTITY => 'Unprocessable Entity.'
        ];
    }
    
    protected function returnDataIfExists($data) {
        if(!$data) {
            return Response::json([
                'error' => 'Not Found.'
            ], 404);
        }
        return Response::json([
            'data' => $data
        ]);
    }
    
    protected function apiError($code) {
        if(array_key_exists($code, $this->getMessages())) {
            $errorMessage = $this->getMessages()[$code];
        } else {
            $errorMessage = 'Unknown Error Message';
        }
        return Response::json([
            'error' => $errorMessage
        ], $code);
    }
    
}
