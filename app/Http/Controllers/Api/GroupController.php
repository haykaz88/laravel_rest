<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Group;
use Response,
    Input,
    Validator,
    Auth;

class GroupController extends Controller
{
    private $actionAddMembers = 'add';
    private $actionRemoveMembers = 'remove';
    
    public function getGroupByIdForUser(Request $request, $id) {
        // in case of showing of only 404 we can use this:
//        $group = Group::whereHas('users', function($query) {
//            $query->where('user_id', $request->user()->id);
//            $query->whereIn('role', [User::GROUP_ROLE_ADMIN, User::GROUP_ROLE_MEMBER]);
//            $query->orWhere('groups.status', Group::STATUS_PUBLIC);
//        })->with('admins', 'members')
//        ->find($id);
        
        $checkGroup = $this->getGroupOrError($id, $request->user()->id, [User::GROUP_ROLE_ADMIN, User::GROUP_ROLE_MEMBER], ['admins', 'members']);
        if(array_key_exists('error', $checkGroup)) {
            return $this->apiError($checkGroup['error']);
        } elseif(array_key_exists('data', $checkGroup)) {
            $group = $checkGroup['data'];
            
            return Response::json([
                'data' => $group
            ]);
        }
    }
    
    public function putUpdateGroupAsUser(Request $request, $id = null) {
        // in case of showing of only 404 we can use this:
//        $group = $this->getGroupWithCheckingOfAdmin(Input::get('id'), $request->user()->id);
        
        if($id) {
            $checkGroup = $this->getGroupOrError($id, $request->user()->id, [User::GROUP_ROLE_ADMIN]);
            if(array_key_exists('error', $checkGroup)) {
                return $this->apiError($checkGroup['error']);
            }
        }
        
        $validator = $this->validateGroup($request, $id);
        if ($validator->fails()) {
            $errors = [];
            foreach($validator->errors()->getMessages() as $field => $message) {
                $errors[] = [
                    'field' => $field,
                    'message' => $message[0]
                ];
            }
            
            return Response::json([
                'success' => false,
                'message' => 'Validation Failed.',
                'errors' => $errors,
            ], self::STATUS_UNPROCESSABLE_ENTITY);
        }
        
        if(isset($checkGroup) && array_key_exists('data', $checkGroup)) {
            $group = $checkGroup['data'];
            $group->title = Input::get('title');
            $group->description = Input::get('description');
            $group->status = Input::get('status');
            $group->save();
            return Response::json([
                'success' => true,
                'group' => $group,
            ]);
        }
        return Response::json([
            'success' => false,
        ], self::STATUS_NO_CONTENT);
    }
    
    private function validateGroup($request, $id = null) {
        $rules = [
            'title' => 'required|min:2|max:100|unique:groups,title',
            'description' => 'required|max:2000',
            'status' => 'required|in:'.Group::STATUS_PUBLIC.','.Group::STATUS_PRIVATE,
        ];
        if($id) {
            $rules['title'] = 'required|min:2|max:100|unique:groups,title,'.$id;
        }
        return Validator::make($request->all(), $rules);
    }


    public function postAddGroupMembersAsUser(Request $request, $id) {
        return $this->changeGroupMembersAsUser($id, $request, $this->actionAddMembers);
    }
    
    public function removeGroupMembersAsUser(Request $request, $id) {
        return $this->changeGroupMembersAsUser($id, $request, $this->actionRemoveMembers);
    }
    
    private function getGroupWithCheckingOfAdmin($id, $userId) {
        return Group::whereHas('users', function($query) use($userId) {
            $query->where('user_id', $userId);
            $query->where('role', User::GROUP_ROLE_ADMIN);
        })
        ->find($id);
    }
    
    private function getGroupOrError($id, $userId, $roles, $relations = null) {
        if(!$relations) {
            $group = Group::find($id);
        } else {
            $group = Group::with($relations)->find($id);
        }
        if(!$group) {
            return ['error' => 404];
        }
        $member = $group->users()
                        ->wherePivotIn('role', $roles)
                        ->find($userId);
        if(!$member) {
            return ['error' => 403];
        }
        return ['data' => $group];
    }
    
    private function changeGroupMembersAsUser($id, $request, $action) {
        // in case of showing of only 404 we can use this:
//        $group = $this->getGroupWithCheckingOfAdmin($id, $request->user()->id);
        
        $checkGroup = $this->getGroupOrError($id, $request->user()->id, [User::GROUP_ROLE_ADMIN]);
        if(array_key_exists('error', $checkGroup)) {
            return $this->apiError($checkGroup['error']);
        }
        
        $rules = [
            'members' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = [];
            foreach($validator->errors()->getMessages() as $field => $message) {
                $errors[] = [
                    'field' => $field,
                    'message' => $message[0]
                ];
            }
            
            return Response::json([
                'success' => false,
                'message' => 'Validation Failed.',
                'errors' => $errors,
            ], self::STATUS_UNPROCESSABLE_ENTITY);
        }
        
        if(array_key_exists('data', $checkGroup)) {
            $group = $checkGroup['data'];
        
            $members = explode(',', $request->members);
            foreach($members as $member) {
                $memberDetails = explode('-', $member);
                if($memberDetails[0] != $request->user()->id) {
                    $this->addOrRemoveMember($group, $action, $memberDetails);
                }
            }
            return Response::json([
                'success' => true,
            ]);
        }
        return Response::json([
            'success' => false,
        ], self::STATUS_NO_CONTENT);
    }
    
    private function addOrRemoveMember($group, $action, $memberDetails) {
        $memberId = $memberDetails[0];
        if($action == $this->actionRemoveMembers) {
            $group->users()->detach([$memberId]);
        } elseif($action == $this->actionAddMembers) {
            $user = User::find($memberId);
            if($user) {
                if(array_key_exists(1, $memberDetails)) {
                    $memberRole = $memberDetails[1];
                }
                if(!isset($memberRole) || !array_key_exists($memberRole, User::getAvailableGroupRoles())) {
                    $memberRole = User::GROUP_ROLE_MEMBER;
                }
                /**
                 * @todo Don't change values of `updated_at` which aren't been changed
                 */
                $group->users()->syncWithoutDetaching([$memberId => ['role' => $memberRole]]);
            }
        }
    }
}
