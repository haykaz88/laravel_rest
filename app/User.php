<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    const GROUP_ROLE_ADMIN = 1;
    const GROUP_ROLE_MEMBER = 2;
    
    public function groups() {
        return $this->belongsToMany('App\Group')
                    ->withTimestamps()
//                    ->withPivot('role')
                ;
    }
    
    public function OauthAcessToken() {
        return $this->hasMany('\App\OauthAccessToken');
    }
    
    public static function getAvailableGroupRoles() {
        return [
            self::GROUP_ROLE_ADMIN => 'Admin',
            self::GROUP_ROLE_MEMBER => 'Member'
        ];
    }
}
