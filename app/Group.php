<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'title', 'description', 'status',
    ];
    
    const STATUS_PUBLIC = 1;
    const STATUS_PRIVATE = 2;
    
    public function users() {
        return $this->belongsToMany('App\User')
                    ->withTimestamps()
                    ->withPivot('role');
    }
    
    public function admins() {
        return $this->belongsToMany('App\User')
                    ->withTimestamps()
                    ->withPivot('role')
                    ->wherePivot('role', User::GROUP_ROLE_ADMIN);
    }
    public function members() {
        return $this->belongsToMany('App\User')
                    ->withTimestamps()
                    ->withPivot('role')
                    ->wherePivot('role', User::GROUP_ROLE_MEMBER);
    }
}
