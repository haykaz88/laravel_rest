<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Group;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $usersCount = $users->count();
        $letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i' , 'j'];
        for($i = 0; $i < count($letters); $i++) {
            $group = new Group;
            $group->title = 'group_'.$letters[$i];
            $group->description = 'group_'.$letters[$i].'\'s description';
            $group->status = (($i % 3) == 0)?Group::STATUS_PUBLIC:Group::STATUS_PRIVATE;
            $group->save();
            
            $group->users()->attach($users[$i % $usersCount]->id, ['role' => User::GROUP_ROLE_ADMIN]);
            
            $restUsers = $users->toArray();
            unset($restUsers[$i % $usersCount]);
            if($i % 3 == 0) {
                $countMembers = 3;
            } else {
                $countMembers = 2;
            }
            $memberKeys = array_rand($restUsers, $countMembers);
            $j = 1;
            foreach($memberKeys as $memberKey) {
                if($j % 3 == 0) {
                    $userRole = User::GROUP_ROLE_ADMIN;
                } else {
                    $userRole = User::GROUP_ROLE_MEMBER;
                }
                $group->users()->attach($restUsers[$memberKey]['id'], ['role' => $userRole]);
                $j ++;
            }
        }
    }
}
