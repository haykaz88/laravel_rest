<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    
    Route::group(['middleware' => 'auth:api'], function () {
        
        Route::group(['prefix' => 'user'], function () {
            Route::get('/', function (Request $request) {
                return json_encode($request->user());
            });
            Route::get('group/{id}', 'Api\GroupController@getGroupByIdForUser');
            Route::put('group/{id}', 'Api\GroupController@putUpdateGroupAsUser');
            Route::post('group/{id}/members', 'Api\GroupController@postAddGroupMembersAsUser');
            Route::delete('group/{id}/members', 'Api\GroupController@removeGroupMembersAsUser');
        });
        
    });
    
});
