Simple REST API based on Laravel 5.4

# API Project Installation

## Install laravel dependencies

```
sudo composer install
```

## Create necessary tables

```
sudo php artisan migrate
```

## Fill users and groups

```
sudo php artisan db:seed
```

## Install node modules

```
sudo npm install
```


# API Usage

Login to api admin panel for example as `user2@example.com` using password `secret`, go to route `api-actions`, create an OAuth Client having Redirect URL `http://your-app.loc/callback`. Then in your your-app.loc in route `callback` have a code like this:

```
$http = new GuzzleHttp\Client;

$response = $http->post('http://laravel-rest.loc/oauth/token', [
    'form_params' => [
        'grant_type' => 'authorization_code',
        'client_id' => 4, // from admin panel above
        'client_secret' => 'EjAyGnmbSznC47oKLV8Kp3NfrvI1rpbDQ3lWraDp', // from admin panel above
        'redirect_uri' => 'http://your-app.loc/callback',
        'code' => $_GET['code'] // Get code from the callback
    ]
]);

// echo the access token
return json_decode((string) $response->getBody(), true)['access_token'];
```

Then get an authentication token like this
```
$query = http_build_query([
    'client_id' => 4,
    'redirect_uri' => 'http://your-app.loc/callback',
    'response_type' => 'code',
    'scope' => 'conference'
]);

// Redirect the user to the OAuth authorization page (function redirect is written for laravel)
return redirect('http://api-url.loc/oauth/authorize?' . $query);
```

After authorization you will be able to see your access token. To test it - send a get request to http://api-url.loc/api/v1/user adding header 'Authorization' 'Bearer your_token'. Please set also 'Content-Type' header to 'application/json' and your 'Accept' header to 'application/json'.


# Dmytro's task

- User should have access to GET specific group with admins and members in case it's public or if he is admin/member of that group
```
GET http://api-url.loc/api/v1/user/group/$groupId
```

- User should have access to PUT specific group in case if he is admin of that group
```
PUT http://api-url.loc/api/v1/user/group/$groupId?title=$title&description=$description&status=$status
```
* $status = 1 --- 'admin'
* $status = 2 --- 'member'

- Add members to specific group in case if user is admin of that group
```
POST http://api-url.loc/api/v1/user/group/$groupId/members?members=$memberId1-$status1,$memberId2-$status2,$memberId3-$status3
```

- Remove members from specific group in case if user is admin of that group
```
DELETE http://api-url.loc/api/v1/user/group/$groupId/members?members=$memberId1,$memberId2,$memberId3
```
